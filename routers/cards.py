from asyncpg.exceptions import UniqueViolationError
from fastapi import APIRouter, BackgroundTasks, Depends, HTTPException, UploadFile
from fastapi.responses import FileResponse

from libs.common import verify_token
from libs.psql import db
from models.cards import Card, NewCard

router = APIRouter(
    tags=["cards"],
    responses={404: {"description": "Not found"}},
)


# @router.get('/')
# async def get_deck_cards(deck_id: int, page: int| None = 1, size: int | None = 50, user_id: int = Depends(verify_token)):
#     r = await db.fetch_one('select user_id from decks where deck_id = :did limit :limit offset :offset', {'did': deck_id})
#     if user_id != r._mapping['user_id']:
#         raise HTTPException(status_code=401)
#     values = {'did': deck_id, 'limit': size, 'offset': (page - 1) * size}
#     return await db.fetch_all('select card_id, black, white, text, fields from cards where deck_id = :did', {'did': values})


@router.post('/')
async def add_card_to_deck(card: NewCard, user_id = Depends(verify_token)):
    r = await db.fetch_one('select user_id from decks where deck_id = :did', {'did': card.deck_id})
    print(r._mapping['user_id'])
    if r._mapping['user_id'] != user_id:
        raise HTTPException(status_code=403)
    black = card.color == 'black'
    values = {'did': card.deck_id, 'b': black, 'w': not black, 't': card.text, 'f': card.fields}
    await db.execute('insert into cards(deck_id, black, white, text, fields) values(:did, :b, :w, :t, :f)', values)
    return card


@router.post('/', response_model=list[Card])
async def import_cards(deck_id: int, cards: list[Card], user_id: int = Depends(verify_token)):
    r = await db.fetch_one('select user_id from decks where deck_id = :did', {'did': deck_id})
    if user_id != r._mapping['user_id']:
        raise HTTPException(status_code=401)
    values = [{'did': deck_id, 'b': i.black, 'w': i.white, 't': i.text, 'f': i.fields} for i in cards]
    try:
        await db.execute_many('insert into cards(deck_id, black, white, text, fields) values(:did, :b, :w, :t, :f)', values)
    except UniqueViolationError:
        raise HTTPException(status_code=400, detail='Card exists in this deck.')
    return cards


async def insert_cards(f, deck_id):
    file_content = await f.read()
    insert_data = []
    for row in file_content.decode().split('\n'):
        card_type, text, fields = row.split(';')

        fields = fields.replace('\r', '')
        if len(fields) == 0 or fields == '0':
            fields = None
        else:
            fields = int(fields)

        black = None
        if card_type == 'black':
            black = True
        elif card_type == 'white':
            black = False

        if black is None:
            raise HTTPException(status_code=400, detail=f'{text} type invalid.')
        insert_data.append({'did': deck_id,'b': black, 'w': not black, 't': text, 'f': fields})

    try:
        await db.execute_many('insert into cards(deck_id, black, white, text, fields) values(:did, :b, :w, :t, :f)', insert_data)
    except UniqueViolationError:
        print('fail')


@router.post('/file-import')
async def import_cards_from_file(deck_id: int, file: UploadFile, background_tasks: BackgroundTasks):
    background_tasks.add_task(insert_cards, file, deck_id)
    return {'message': 'Background task'}


@router.get('/example')
async def download_example():
    return FileResponse('/static/example.csv')


@router.put('/{card_id}')
async def update_card(card_id: int, card: NewCard, user_id: int = Depends(verify_token)):
    card_user_id = await db.fetch_one('select user_id from decks where deck_id = :did', {'did': card.deck_id})
    if user_id != card_user_id._mapping['user_id']:
        raise HTTPException(status_code=403)

    black = card.color == 'black'
    values = {'b': black, 'w': not black, 't': card.text, 'f': card.fields, 'cid': card_id}
    print(values)
    await db.execute('update cards set black = :b, white = :w, text = :t, fields = :f where card_id = :cid', values)
    return {'card_id': card_id}
    

@router.delete('/{card_id}')
async def delete_card(card_id: int, user_id: int = Depends(verify_token)):
    print(card_id)
    card_user_id = await db.fetch_one('select b.user_id from cards a left join decks b on a.deck_id = b.deck_id where a.card_id = :cid', {'cid': card_id})
    print(card_user_id._mapping['user_id'])
    if user_id != card_user_id._mapping['user_id']:
        raise HTTPException(status_code=403, detail='It\'s not your deck.')
    await db.execute('delete from cards where card_id = :cid', {'cid': card_id})
    return {'card_id': card_id}

