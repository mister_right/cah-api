import base64
import os
import uuid

from fastapi import APIRouter, Depends, Header, HTTPException, Request, UploadFile

from libs.common import check_password, get_hashed_password, generate_jwt_token, verify_token
from libs.psql import db
from models.users import NewDescription, UserIn, UserLogin, UserOut

BASE_URL = os.environ.get('BASE_URL', '')

router = APIRouter(
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get('/username')
async def get_username(user_id: int = Depends(verify_token)):
    return await db.fetch_one('select username from users where user_id = :uid', {'uid': user_id})


@router.get('/profile')
async def get_profile(user_id: int = Depends(verify_token)):
    data = await db.fetch_one('select description, img_path, win_sound_path from profiles where user_id = :uid', {'uid': user_id})
    img_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(data._mapping['img_path'].encode()).decode()
    sound_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(data._mapping['win_sound_path'].encode()).decode()
    return {
        'description': data._mapping['description'],
        'img': img_url,
        'sound': sound_url
    }


@router.post('/profile-description')
async def update_description(description: NewDescription, user_id: int = Depends(verify_token)):
    await db.execute('update profiles set description = :d where user_id = :uid', {'d': description.text, 'uid': user_id})
    return {'description': description.text}


@router.post('/profile-image')
async def update_image(image: UploadFile, user_id: int = Depends(verify_token)):
    profile_id = await db.fetch_one('select profile_id from profiles where user_id = :uid', {'uid': user_id})
    profile_id = profile_id._mapping['profile_id']
    dir_path = f'/static/profiles/{profile_id}'
    if os.path.exists(dir_path) is False:
        os.mkdir(dir_path)

    filepath = f'{dir_path}/{image.filename}'
    with open(filepath, 'wb') as f:
        f.write(image.file.read())

    await db.execute('update profiles set img_path = :path where profile_id = :pid', {'path': filepath, 'pid': profile_id})
    new_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(filepath.encode()).decode()
    return {'url': new_url}


@router.post('/profile-sound')
async def update_sound(sound: UploadFile, user_id: int = Depends(verify_token)):
    profile_id = await db.fetch_one('select profile_id from profiles where user_id = :uid', {'uid': user_id})
    profile_id = profile_id._mapping['profile_id']
    dir_path = f'/static/profiles/{profile_id}'
    if os.path.exists(dir_path) is False:
        os.mkdir(dir_path)

    filepath = f'{dir_path}/{sound.filename}'
    print(filepath)
    with open(filepath, 'wb') as f:
        f.write(sound.file.read())

    await db.execute('update profiles set win_sound_path = :path where profile_id = :pid', {'path': filepath, 'pid': profile_id})
    new_url = BASE_URL + '/api/file/' + base64.urlsafe_b64encode(filepath.encode()).decode()
    return {'url': new_url}


@router.post('/create', status_code=201, response_model=UserOut)
async def create_user(user: UserIn):
    if len(user.username) == 0:
        raise HTTPException(status_code=400, detail="Invalid username.")

    if user.password0 != user.password1:
        raise HTTPException(status_code=400, detail="Passwords do not match.")

    a = await db.fetch_one('select 1 from users where username = :u', {'u': user.username})
    if a is not None:
        raise HTTPException(status_code=400, detail="Username taken.")
    
    hashed_password = get_hashed_password(user.password0)
    user_id = await db.fetch_one('insert into users(username, password) values(:u, :p) returning user_id', {'u': user.username, 'p': hashed_password})
    await db.execute('insert into profiles(user_id) values(:u)', {'u': user_id._mapping['user_id']})
    return user


@router.post('/login')
async def login(user: UserLogin):
    data = await db.fetch_one('select user_id, password from users where username = :u', {'u': user.username})
    if data is None:
        raise HTTPException(status_code=403, detail='Invalid username or password.')
    if check_password(user.password, data._mapping['password']) is False:
        raise HTTPException(status_code=403, detail='Invalid username or password.')
    token = generate_jwt_token(data._mapping['user_id'])
    return {'token': token, 'username': user.username}
