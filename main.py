import base64
import os

from fastapi import HTTPException, FastAPI, status
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse

from libs.psql import db
from routers import cards, decks, games, users

app = FastAPI()

@app.on_event("startup")
async def startup():
    await db.connect()


@app.on_event("shutdown")
async def shutdown():
    await db.disconnect()


app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=['*'],
    allow_headers=['*'],
)

app.include_router(cards.router, prefix='/api/cards')
app.include_router(decks.router, prefix='/api/decks')
app.include_router(games.router, prefix='/api/games')
app.include_router(users.router, prefix='/api/users')


@app.get('/api/file/{encoded_path}')
async def get_file(encoded_path: str):
    str_path = base64.urlsafe_b64decode(encoded_path).decode()
    if os.path.exists(str_path) is False:
        raise HTTPException(status_code=404, detail='File does not exist.')
    return FileResponse(str_path)
